package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class UserPage {
	WebDriver driver = null;
	
	WebDriver ldriver;
	
	public UserPage(WebDriver rdriver) {
		this.ldriver = rdriver;
		
		PageFactory.initElements(rdriver, this);
	}
	
	//@FindBy(xpath = "//select[@id='userSelect']")
	//WebElement UserSelection;
	
	public void SelectUser(String customerName) {
		//UserSelection.click();
		
		Select userSelect = new Select(driver.findElement(By.xpath(".//label[contains(text(),'Your Name :')]//following::select[1]")));
		userSelect.selectByValue(customerName);
	}
	
	@FindBy(xpath = ".//button[contains(text(),'Login')]")
	WebElement Login;
	
	public void ClickBtnLogin() throws InterruptedException {
		Login.click();
		Thread.sleep(2000);
	}

}
