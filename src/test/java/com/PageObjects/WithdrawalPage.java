package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WithdrawalPage {
	
	WebDriver driver = null;
	
	WebDriver ldriver;
	
	public WithdrawalPage(WebDriver rdriver) {
		this.ldriver = rdriver;
		
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath="//label[contains(text(),'Amount to be Withdrawn :')]//following::input[1]")
	WebElement WithdrawalField;
	
	@FindBy(xpath = "//button[@type='submit']")
	WebElement BtnWithdraw;
	
	public void EnterWithdrawalAmount(String amount) {

		WithdrawalField.clear();
		WithdrawalField.sendKeys(amount);
	}
	
	public void ClickBtnWithdraw() {
		
		BtnWithdraw.click();
	}

}
