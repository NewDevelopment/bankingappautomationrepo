package com.TestCases;

import org.testng.annotations.Test;

import com.PageObjects.AccountPage;
import com.PageObjects.HomePage;
import com.PageObjects.TransactionPage;
import com.PageObjects.UserPage;
import com.PageObjects.WithdrawalPage;

public class TC_Way2TestCases extends BaseClass {
	
	@Test
	
	public void test1() {
		try {
			driver.get(BaseUrl);

			HomePage homepage = new HomePage(driver);
			homepage.ClickCustomerLogin();
			UserPage userpage = new UserPage(driver);
			userpage.SelectUser("Hermoine Granger");
			userpage.ClickBtnLogin();
			AccountPage accpage = new AccountPage(driver);
			accpage.ClickBtnDeposit();
			accpage.EnterDepositAmount("1500");
			accpage.ClickDeposit();
			accpage.VerifyStatusMessage("Deposit Successful");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	@Test(dataProvider = "accNaccNumberDataumberData", dataProviderClass = AccountNumberDataProvider.class)
	public void Test2(String accNumber) {
		
		try {
			driver.get(BaseUrl);

			HomePage homepage = new HomePage(driver);
			homepage.ClickCustomerLogin();
			UserPage userpage = new UserPage(driver);
			userpage.SelectUser("Hermoine Granger");
			userpage.ClickBtnLogin();
			AccountPage accpage = new AccountPage(driver);
			accpage.SelectAccountNumber(accNumber);
			accpage.ClickBtnDeposit();
			accpage.EnterDepositAmount("1500");
			accpage.ClickDeposit();
			accpage.VerifyStatusMessage("Deposit Successful");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	
	public void Test3() {
		try {
			driver.get(BaseUrl);

			HomePage homepage = new HomePage(driver);
			homepage.ClickCustomerLogin();
			UserPage userpage = new UserPage(driver);
			userpage.SelectUser("Ron Weasly");
			userpage.ClickBtnLogin();
			AccountPage accpage = new AccountPage(driver);
			accpage.ClickBtnDeposit();
			accpage.EnterDepositAmount("31459");
			accpage.ClickDeposit();
			accpage.VerifyStatusMessage("Deposit Successful");
			accpage.ClickBtnTransactions();
			TransactionPage transpage = new TransactionPage(driver);
			transpage.VerifyTransactionAmount("31459");
			transpage.ClickBtnBack();
			accpage.ClickBtnWithdrawal();
			WithdrawalPage withdrawalpage = new WithdrawalPage(driver);
			withdrawalpage.EnterWithdrawalAmount("31459");
			withdrawalpage.ClickBtnWithdraw();
			accpage.VerifyStatusMessage("Transaction successful");
			accpage.VerifyCurrentBalance("0");
			accpage.ClickBtnTransactions();
			transpage.VerifyTransactionType("Debit");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void Test4() {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
