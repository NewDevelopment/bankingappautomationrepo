package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class TransactionPage {
	
	WebDriver driver = null;
	
	WebDriver ldriver;
	
	public TransactionPage(WebDriver rdriver) {
		this.ldriver = rdriver;
		
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath= "//button[contains(text(),'Back')]")
	WebElement BtnBack;
	
	public void ClickBtnBack() {
		
		BtnBack.click();
	}
	
	public void VerifyTransactionAmount(String transAmount) {
		
		String transactionAmount = driver.findElement(By.xpath("//a[contains(text(),'Amount')]//following::tr[1]/td[2]")).getText();
		
		Assert.assertTrue(transactionAmount.contains(transAmount));
	}
	
	public void VerifyTransactionType(String transType) {
		
		String transactionAmount = driver.findElement(By.xpath("//a[contains(text(),'Transaction Type')]//following::tr[2]/td[3]")).getText();
		
		Assert.assertTrue(transactionAmount.contains(transType));
	}


}
