package com.TestCases;

import org.testng.annotations.DataProvider;

public class AccountNumberDataProvider {
	
	@DataProvider
	public Object[][] accNumberData(){
		return new Object[][] {
			{"1001"},
			{"1002"},
			{"1003"}
				
		};
	}

}
