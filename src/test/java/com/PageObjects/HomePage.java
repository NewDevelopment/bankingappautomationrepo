package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	WebDriver ldriver;
	
	public HomePage(WebDriver rdriver) {
		
		this.ldriver = rdriver;
		
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath = "//button[contains(text(),'Customer Login')]")
	@CacheLookup
	WebElement CustomerLogin;
	
	
	public void ClickCustomerLogin() {
		
		CustomerLogin.click();
	}

}
