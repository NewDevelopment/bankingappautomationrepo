package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class AccountPage {

	WebDriver driver = null;
	WebDriver ldriver;

	public AccountPage(WebDriver rdriver) {
		this.ldriver = rdriver;

		PageFactory.initElements(rdriver, this);
	}

	@FindBy(xpath = "//button[contains(text(),'Deposit')]")
	WebElement BtnDeposit1;

	@FindBy(xpath = "//label[contains(text(),'Amount to be Deposited :')]//following::input[1]")
	WebElement AmountField;

	@FindBy(xpath = "//button[@class='btn btn-default']")
	WebElement BtnDeposit2;
	
	@FindBy(xpath ="//button[contains(text(),'Transactions')]")
	WebElement BtnTransaction;
	
	@FindBy(xpath = "//button[contains(text(),'Withdrawl')]")
	WebElement BtnWithdrawal;

	public void ClickBtnDeposit() {
		BtnDeposit1.click();
	}

	public void EnterDepositAmount(String amount) {

		AmountField.clear();
		AmountField.sendKeys(amount);
	}

	public void ClickDeposit() {
		BtnDeposit2.click();
	}

	public void SelectAccountNumber(String accnumber) {
		
		Select accNumberSelect = new Select(driver.findElement(By.xpath(".//*[@id='accountSelect']")));
		accNumberSelect.selectByValue(accnumber);

	}
	
	public void ClickBtnTransactions() {
		
		BtnTransaction.click();
	}
	
	public void ClickBtnWithdrawal() {
		
		BtnWithdrawal.click();
	}
	
	public void VerifyCurrentBalance(String balanceAmount) {
		
		String currentBalance = driver.findElement(By.cssSelector(".ng-binding:nth-child(2)")).getText();
		
		Assert.assertTrue(currentBalance.contains(balanceAmount));
	}

	public void VerifyStatusMessage(String statusMessage) {
		String ExpectedStatusMessage = statusMessage;
		WebElement ActualStatusMessage = driver.findElement(By.xpath("//span[@class='error ng-binding']"));
		Assert.assertEquals(ActualStatusMessage, ExpectedStatusMessage);
	}
}
