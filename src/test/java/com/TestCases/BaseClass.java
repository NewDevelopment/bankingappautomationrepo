package com.TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseClass {
	
	WebDriver driver = null;
	String BaseUrl = "http://www.way2automation.com/angularjs-protractor/banking/#/login";
	
	@BeforeTest
	public void setUp() {
		
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+ "/Drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
